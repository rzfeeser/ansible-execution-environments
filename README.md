# Ansible Execution Environments (EEs)

## Introduction

This repository houses Ansible Execution Environments (EEs) suitable for use with Ansible Controller (Tower) or Ansible AWX.


## Getting Started

The `.gitlab-ci.yml` file in this repository will cause automation to build and commit EEs to the GitLab registry as changes are made to the code base.


## Resources
- [Ansible Programming Language](https://docs.ansible.com/)
- [AWX](https://github.com/ansible/awx)
- [GitLab - Pipelines](https://docs.gitlab.com/ee/ci/pipelines/)
- [GitLab - .gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html)
- [GitLab - .gitlab-ci.yml Keyword Reference](https://docs.gitlab.com/ee/ci/yaml/)


## Author
@RZFeeser - Author & Instructor - Feel free to reach out to if you're looking for a instructor led training solution.
